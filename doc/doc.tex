\documentclass[twoside]{article}


% ------
% Fonts and typesetting settings
\usepackage[sc]{mathpazo}
\usepackage[T1]{fontenc}
\linespread{1.05} % Palatino needs more space between lines
\usepackage{microtype}

\usepackage{graphicx}

% ------
% Page layout
\usepackage[hmarginratio=1:1,top=32mm,columnsep=20pt]{geometry}
\usepackage[font=it]{caption}
\usepackage{paralist}
\usepackage{multicol}
\usepackage{amsmath}
\usepackage{needspace}
\usepackage{picins}
\usepackage[usenames,dvipsnames]{color}

% ------
% Lettrines
\usepackage{lettrine}


% ------
% Abstract
\usepackage{abstract}
	\renewcommand{\abstractnamefont}{\normalfont\bfseries}
	\renewcommand{\abstracttextfont}{\normalfont\small\itshape}


% ------
% Titling (section/subsection)
\usepackage{titlesec}
\titleformat{\section}[block]{\large\scshape\centering{\Roman{section}.}}{}{1em}{}


% ------
% Header/footer
\usepackage{fancyhdr}
	\pagestyle{fancy}
	\fancyhead{}
	\fancyfoot{}
	\fancyhead[C]{$\bullet$ May 2012 $\bullet$}
	\fancyfoot[RO,LE]{\thepage}


% ------
% Clickable URLs (optional)
\usepackage{hyperref}

% ------
% Maketitle metadata
\title{\vspace{-15mm}%
	\fontsize{24pt}{10pt}\selectfont
	\textbf{RATEPIC - A SOCIAL INFLUENCE EXPERIMENT}
	}	
\author{%
	\large
	\textsc{Omar Shammas and Lars Kristian Johansen} \\[2mm]
	\vspace{-5mm}
	}
\date{}



%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

\maketitle
\thispagestyle{fancy}

\newcommand{\ItemImage}[5] {
	\pichskip{#5 in}\parpic[#2][#3]{
		\includegraphics[width=68mm]{#1.png}
	}
}

\newcommand{\ItemDescription}[1]%
{\noindent\color{black}#1\newline}

\begin{multicols*}{2}

\lettrine[nindent=0em,lines=3]{T} his paper describes our photo liking experiment that aims to determine how susceptible individuals are to social influence. Our photo experiments determine the bias that displaying the like count of a picture has on individual preferences. The results of the experiment has much wider implications and reveals key insights into human nature. 1858 individuals participated in the experiment acquired through marketing on platforms such as Facebook, Twitter, Reddit, and Hacker News. The paper is separated into three main sections. We begin by describing the experiment design, followed by an analysis of our results. Lastly, a conclusion that discusses the key takeaways.
\section{Experiment}
We conducted a web experiment to test how social influence can affect individual decisions. Inspired by the experiment of Salganik, et. al.\cite{matthew} where they built a music download site enabling users to listen, rate, and download songs. Similarly, we built a photo liking website called RatePic (\url{http://ratepic.netau.net}). 

Salganik, et al. designed their experiment to study how 48 obscure songs of varying quality rise in popularity with and without social influence. Our experiment takes a different approach and studies the rich-get-richer effects with and without social influence once the subset of rich is already determined. This led to several modifications, but most importantly all of the photos used in the experiment were of comparable quality eliminating as much bias as possible that could affect the results. The pictures are credited to Trey Ratcliff\cite{trey} and Salvador Dali\cite{salvador}.

We considered using songs or videos, but these forms of media requires significantly more effort on the user's part, and as a result our experiment would require more users to gather enough data. We settled on photos instead to complete the project within our time constraints.
\subsection*{Layout}
Users are presented with twenty photos, their respective like counts and a button to like as shown in Figure 1. The pictures are each displayed in a grid format with two pictures on every row, and  a total of ten rows as shown in Figure 2. \newline

\ItemImage{structure}{sr}{c}{1.25}{1.25}
\ItemDescription{\newline \newline \newline \newline \newline \newline \newline \newline \newline \newline \newline Figure 1: A single picture's structure.}
\subsection*{Design}
Our experiment was designed to understand how users are affected by social influence. Every user would be presented twenty photos in the layout described in the previous section, and are given the opportunity to like the photo. Users are only able to vote once per photo, to ensure one user can't bias the entire experiment.  

\ItemImage{layout}{sr}{c}{1.25}{1.25}
\ItemDescription{\newline \newline \newline \newline \newline \newline \newline \newline \newline \newline \newline  \newline \newline \newline \newline \newline \newline \newline \newline \newline \newline \newline Figure 2: The pictures layout on the website.}

When a user visits our website they are placed in one of two groups, a test or control group. Users in the test group are presented with the like count of each picture. For users in the control group, the like count is simply omitted. See appendix for pictures.

To test how social influence affects individual decisions we randomly assigned \textit{like} counts to pictures, which are of comparable quality. We divide the pictures into four categories:
\begin{compactitem}
\item number one picture - 1 picture with  900$\pm$100likes
\item top pictures - 2 to 6 pictures with 500$\pm$100likes
\item middle pictures - 7 to 12 pictures with 250$\pm$100likes
\item bottom pictures - all the remaining photos with 50$\pm$50likes
\end{compactitem}

Every picture has an equal probability of being assigned to any of these categories. It is important to note that this assignment process is repeated for every new user. Users are placed in their own separate world, and the decisions of one user is not introduced into the world of another.

The pictures are ranked in descending order by like count and then placed in their respective positions in the layout shown in Figure 2.  Intuitively, we expected there to be a greater bias for pictures in the upper portion of the website compared to those on the bottom, however, the difference proved to be much smaller than anticipated (explained in further detail in the results section).

\subsection*{Execution}
To be able to run the experiment we built the website using PHP with a MySQL backend hosted by 000webhost.com. Having a simple and clean interface was critical to ensure users interact with the website. 

Once the technical component was complete, the next task was marketing the website to gather enough data. This was extremely challenging and to accomplish this we leveraged several platforms such as Facebook, Twitter, Reddit and Hacker News. In the end,  1858 people visited the website with an accumulated 1887 votes. 
\section{Results}

Figure 3  shows the total number of votes each picture received individually labelled from A to T. As seen there is a strong correlation between the results from the control group and the test group, which was expected. The plots are scaled to be able to easily compare the two groups since the test group was assigned a larger number of users than the control group. The results show that all pictures had approximately similar votes, due to comparable picture quality which was as expected. However, there is one picture that received significantly more votes than others in the test group. This outlier is attributed to the bias added when promoting on Facebook, which required a picture together with the link. The outstanding picture was the chosen picture, and hence it got more votes than the others.

\ItemImage{combined}{sr}{c}{1.25}{1.25}
\ItemDescription{\newline \newline \newline \newline \newline \newline \newline \newline \newline \newline \newline \newline \newline Figure 3: Combined test and control group graph based on votes per picture.}

Figure 4 shows the number of votes received in the control group. The pictures are ordered randomly, i.e. any of pictures from A to T could have been placed in any of the positions 1 to 20, referring to Figure 2. People tend to like pictures almost uniformly under the control group's conditions. This graph presents two interesting unexpected results. First, the ordering of pictures had minimal effect on people which indicates that people were as likely to vote for the picture in position 1 as they would for a picture in position 20 which is counter intuitive.
\ItemImage{control_biased}{sr}{c}{1.25}{1.25}
\ItemDescription{\newline \newline \newline \newline \newline \newline \newline \newline \newline \newline \newline \newline \newline \newline Figure 4: Control group graph based on votes for pictures in position 1 to 20.}

Second, pictures on the right side of the website (the even number on the plot) received more votes than pictures on the left. This insight could be further studied in other experiments to better understand human computer interaction but it is not the aim of this paper.

Figure 5 shows the same plot as Figure 4, but using data from the test group. Clearly, there is a big difference between those two plots. People in the test category tended to vote for the pictures that were popular. Recall that the only difference between the test group and the control group is that the vote count is visible to users in the test group.  This data clearly indicates how susceptible users are to this feedback loop and hence enforce the rich-get-richer scheme.

\ItemImage{test_biased}{sr}{c}{1.25}{1.25}
\ItemDescription{\newline \newline \newline \newline \newline \newline \newline \newline \newline \newline \newline \newline \newline \newline Figure 5: Test group graph based on votes for pictures in position 1 to 20. (Pictures ordered by popularity)}

To negate the bias in the results produced by users tendencies to vote for pictures on the right hand side of the website we divide the data of the test group by the normalized data of the control group. Figure 6 which has removed these effects, shows the pure effect that displaying the vote counts has on an individual's decision. There is a clear decreasing pattern in the number of votes and popularity of the picture. Given enough users the curve will be even more pronounced.
\ItemImage{test_unbiased}{sr}{c}{1.25}{1.25}
\ItemDescription{\newline \newline \newline \newline \newline \newline \newline \newline \newline \newline \newline \newline \newline \newline \newline Figure 6: Normalized data of graph in Figure 5.}
\section{Conclusion}
The results discussed in the paper are quite exciting and provide a good insight into human nature. Most people would not like to believe that we are easily influenced - but clearly our subconscious picks up on these clues. In our experiment this bias was as small as displaying the vote count, but the thought of all the subtle clues that are influencing us in our daily life that we are not necessarily aware of is both quite frightening and intriguing.\newline \newline \newline \newline \newline \newline \newline \newline \newline \newline \newline \newline \newline \newline \newline \newline \newline

\begin{thebibliography}{9}

\bibitem{matthew}
  Matthew J. Salganik, Peter Sheridan Dodds, Duncan J. Watts,
  \emph{Experimental Study of Inequality and Unpredictability in an Artificial Cultural Market}. \\
  \url{http://www.princeton.edu/~mjs3/salganik_dodds_watts06_full.pdf}
  
\bibitem{trey}
  Trey Ratcliff
  \emph{} \\
  \url{http://www.stuckincustoms.com/}
  
\bibitem{salvador}
  Salvador Dali
  \emph{} \\
  \url{http://en.wikipedia.org/wiki/Salvador_Dali}

\end{thebibliography}

\section{Appendix}

\ItemImage{control}{sr}{c}{1.25}{1.25}
\ItemDescription{\newline \newline \newline \newline \newline \newline \newline \newline \newline \newline \newline \newline \newline Figure 7: Website seen from user in control group}

\ItemImage{test}{sr}{c}{1.25}{1.25}
\ItemDescription{\newline \newline \newline \newline \newline \newline \newline \newline \newline \newline \newline \newline \newline Figure 8: Website seen from user in test group}

\end{multicols*}

\end{document}

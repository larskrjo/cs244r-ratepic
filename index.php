<?php

// $mysql_host = 'mysql7.000webhost.com';
// $mysql_database = 'a8339835_picture';
// $mysql_user = 'a8339835_picture';
// $mysql_password = 'zxcvbnm1234567890';
$mysql_host = 'localhost';
$mysql_database = 'test';
$mysql_user = 'root';
$mysql_password = '';

$con = mysql_connect($mysql_host, $mysql_user, $mysql_password);

if (!$con) {
	die('Could not connect: ' . mysql_error());
}

// mysql_select_db("rating", $con);
mysql_select_db($mysql_database, $con);

// Initial user id
$user_id = 0;

if (isset($_COOKIE[user])) { // User been here before
	$user_id = $_COOKIE[user];
}
else { // New user

	$top = rand(2, 6);
	$middle = rand(7, 12);
	$bottom = 20 - $top - $middle;

	$array = array();
	$array[] = 900 + rand(-100,100);
	for($i = 0; $i < $top-1; $i++){
		$array[] = 500 + rand(-100,100);
	}
	for($i = 0; $i < $middle; $i++){
		$array[] = 250 + rand(-100,100);
	}
	for($i = 0; $i < $bottom; $i++){
		$array[] = 50 + rand(0,50);
	}
	shuffle($array);

	$sql = "INSERT INTO rating (user_id, picture_id, votes, original) VALUES(id+1, 0, ".$array[0].", 1);";
	mysql_query($sql);
	$user_id = mysql_insert_id();
	$sql = "UPDATE rating SET user_id = ".$user_id." WHERE id=".$user_id.";";
	mysql_query($sql);
	$sql = "INSERT INTO rating (user_id, picture_id, votes, original) VALUES(".$user_id.", 0, ".$array[0].", 0);";
	mysql_query($sql);

	for($i = 1; $i < 20; $i++) {
		$sql = "INSERT INTO rating (user_id, picture_id, votes, original) VALUES(".$user_id.", ".$i.", ".$array[$i].", 1);";
		mysql_query($sql);
		$sql = "INSERT INTO rating (user_id, picture_id, votes, original) VALUES(".$user_id.", ".$i.", ".$array[$i].", 0);";
		mysql_query($sql);
	}

	  // Set the user cookie
	$expire=time()+60*60*24*30;
	setcookie("user", $user_id, $expire);
}

$sql = "SELECT * FROM rating WHERE user_id=".$user_id." AND original=0 ORDER BY votes DESC, RAND()";
$result = mysql_query($sql);

?>

<html>
<head>
	<title>RatePic</title>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
	<script type="text/javascript" src="./fancybox/jquery.fancybox-1.3.4.pack.js"></script>
	<link rel="stylesheet" type="text/css" href="./fancybox/jquery.fancybox-1.3.4.css" media="screen" />
	<script type="text/javascript">

	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-31381517-1']);
	_gaq.push(['_trackPageview']);

	(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();

	</script>
	<script>

	$(document).ready(function() {

		function send_ajax(pic_id_number){
			$.ajax({
				type: "POST",
				url: "vote.php",
				data: { 
					user_id: <?= $user_id ?>, pic_id: pic_id_number
				}
			}).done(function( msg ) {
				alert( "Thank you for the vote!");
			});
			return false;
		}

		function updateVotes(votesIdNumber, likeIdNumber) {
			var votes = $(votesIdNumber).html();
			console.log(votes);
			votes = parseInt(votes);
			console.log(votes);
			$(votesIdNumber).html(votes+1);
			$(likeIdNumber).remove();
		}
		<?php 
		for($i = 0; $i < 20; $i++) {
			echo "$(\"a#pic".$i."\").fancybox();";
			echo "$(\"#like".$i."\").click(function () {
				updateVotes(\"#votes".$i."\", \"#like".$i."\");
				send_ajax(".$i.");
			});";
} ?>
});
	</script>

	<style type="text/css">

	body{
		background: #ffffff;
		font-family: Arial;
		font-size: 11px;
		color: #535353;
	}

	h1{
		font-size: 5em;
		font-weight:normal;
		margin: 0px;
	}

	h2 {
		margin:0px;
	}

	a{ color: #000; text-decoration: none;}
	a:hover{ color: #eee;}


/*#container, #header, #body, #footer, .photoLeft, .photoRight, .row{
	border: 2px solid;
	}*/

	#container{
		margin-left: auto;
		margin-right: auto;
		width: 720px;
		margin-top: 30px;
	}

	#header {
		margin:0;
		padding:0;
	}

	#body {
		margin-top: 50px;

	}

	.row {
		width: 720px;	
		height: 275px;
		margin-bottom:40px;
		font-weight:bold;
		font-size:14px;
	}

	.photoLeft {
		float: left;
	}

	.photoRight {
		float: right;
		margin:none;
		padding:none;
	}

	.photo {
		width: 320px;	
		height: 220px;
		overflow: hidden;
		-webkit-box-shadow: 0px 0px 3px rgba(0, 0, 0, 0.4);
		-moz-box-shadow: 0px 0px 3px rgba(0, 0, 0, 0.4);
		border-radius: 10px;
		-moz-border-radius: 10px;
		-webkit-border-radius: 10px;
	}

	.like {
		float: right;
		-moz-box-shadow:inset 0px 0px 0px 0px #ffffff;
		-webkit-box-shadow:inset 0px 0px 0px 0px #ffffff;
		box-shadow:inset 0px 0px 0px 0px #ffffff;
		background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #ededed), color-stop(1, #dfdfdf) );
		background:-moz-linear-gradient( center top, #ededed 5%, #dfdfdf 100% );
		filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ededed', endColorstr='#dfdfdf');
		background-color:#ededed;
		-moz-border-radius:6px;
		-webkit-border-radius:6px;
		border-radius:6px;
		border:1px solid #dcdcdc;
		display:inline-block;
		color:#535353;
		font-family:arial;
		font-size:14px;
		font-weight:bold;
		padding:6px 24px;
		text-decoration:none;
		text-shadow:1px 1px 0px #ffffff;
	}
	.like:hover {
		background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #dfdfdf), color-stop(1, #ededed) );
		background:-moz-linear-gradient( center top, #dfdfdf 5%, #ededed 100% );
		filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#dfdfdf', endColorstr='#ededed');
		background-color:#dfdfdf;
	}
	.like:active {
		position:relative;
		top:1px;
	}

	#footer {
		margin-top:20px;
		text-align:center;
		font-weight:bold;
	}

	.clear {
		clear:both;
	}


	</style>
</head>
<body>
	<div id="container">
		<div class="header">
			<h1>RatePic</h1>
			<h2>A machine learning experiment</h2>
		</div>

		<div id="body"> <?php
		while($row = mysql_fetch_array($result)) {
			?>
			<div class="row">
				<div class="photoLeft">
					<div class="photo">
						<a id="pic<?=$row['picture_id']?>" href="pictures/<?=$row['picture_id']?>.jpg">
							<img src="pictures/<?=$row['picture_id']?>.jpg" width="350px" alt="pic<?=$row['picture_id']?>">
						</a>
					</div>
					<p>	
						<?php 
						if($user_id < 56761 || $user_id > 70320) {
							?>
							<img src="pictures/like.png" alt="Like">
							<span id="votes<?=$row['picture_id']?>"><?= $row['votes'] ?></span> Upvotes
							<?php
						}
						if(!isset($_COOKIE["like_pic_id".$row['picture_id']])) {
							echo "<a id=\"like".$row['picture_id']."\" class=\"like\">Like</a>";
						} ?>
					</p>
				</div>
				<?php
				$row = mysql_fetch_array($result);
				?>
				<div class="photoRight">
					<div class="photo">
						<a id="pic<?=$row['picture_id']?>" href="pictures/<?=$row['picture_id']?>.jpg">
							<img src="pictures/<?=$row['picture_id']?>.jpg" width="350px" alt="pic<?=$row['picture_id']?>">
						</a>
					</div>
					<p>	<?php 
						if($user_id < 56761 || $user_id > 70320) {
							?>
							<img src="pictures/like.png" alt="Like">
							<span id="votes<?=$row['picture_id']?>"><?= $row['votes'] ?></span> Upvotes
							<?php
						}
						if(!isset($_COOKIE["like_pic_id".$row['picture_id']])) {
							echo "<a id=\"like".$row['picture_id']."\" class=\"like\">Like</a>";
						} ?>
					</p>
				</div>
			</div>
			<?php } ?>
		</div>

		<div class="clear"></div>

		<div id="footer">
			<div class="center">
				&copy; 2012 RatePic - Images by Trey Ratcliff at <a href="http://www.stuckincustoms.com">stuckincustoms.com</a>
			</div>
		</div>
	</div>
</body>
</html>
<?php mysql_close($con); ?>